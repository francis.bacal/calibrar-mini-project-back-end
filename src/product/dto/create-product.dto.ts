import { IsDate, IsInt, IsNotEmpty } from 'class-validator';

export class CreateProductDto {
    @IsNotEmpty()
    id: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    @IsInt()
    price: number;

    ingredients?: string[]

    recipe?: string;

    @IsDate()
    date_created?: Date;

    @IsDate()
    date_updated?: Date;

}