import { Body, Controller, Delete, Get, HttpException, HttpStatus, Logger, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { hasRoles } from 'src/auth/decorator/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Product } from '../dto/product.dto';
import { ProductService } from '../service/product.service';


@Controller('product')
export class ProductController {

    constructor(private readonly productService: ProductService) { }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Post()
    create(@Body() product: Product) {
        return this.productService.create(product).pipe(
            catchError(err => {
                let message: string;
                if (err.code && err.code == 23505) {
                    message = "Duplicate value found in database";
                } else {
                    message = "Unknown error"
                }
                throw new HttpException(`Error occured: ${message}`, HttpStatus.BAD_REQUEST);
            })
        )
    }

    @Get(':id') 
    findById(@Param('id') id: string): Observable<Product> {
        return this.productService.findById(id);
    }

    @Get()
    findAll(): Observable<Product[]> {
        return this.productService.findAll();
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Delete(':id')
    delete(@Param('id') id: string): Observable<Product> {
        console.log(id)
        return this.productService.delete(id)
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Put(':id')
    update(@Param('id')id: string, @Body() product: Product): Observable<any> {
        return this.productService.update(id, product);
    }
}
