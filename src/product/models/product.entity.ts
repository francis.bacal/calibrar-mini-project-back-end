import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Products {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column( { 
        unique: true,
        nullable: false
    } )
    name: string;

    @Column( {
        nullable: false,
        default: 0
    } )
    price: number;

    @Column( {
        type: "varchar",
        nullable: false,
        array: true,
        default: "{''}"
    } )
    ingredients: string[];

    @Column( {
        type: 'text',
        nullable: true
    } )
    recipe: string;

    @CreateDateColumn()
    date_created: Date;

    @UpdateDateColumn()
    date_updated: Date;
}