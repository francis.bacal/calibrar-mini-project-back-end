import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { Products } from '../models/product.entity';
import { Product } from '../dto/product.dto';
import { CreateProductDto } from '../dto/create-product.dto';

@Injectable()
export class ProductService {

    constructor(
        @InjectRepository(Products) private readonly productRepository: Repository<Products>,
    ) {}

    create(product: CreateProductDto): Observable<CreateProductDto> {
        return from(this.productRepository.save(product));
    }

    findAll(): Observable<Product[]> {
        return from(this.productRepository.find());
    }

    findById(id: string): Observable<Product> {
        return from(this.productRepository.findOne( { id: id } ));
    }

    delete(id: string): Observable<any> {
        return from(this.productRepository.delete(id));
    }

    update(id: string, product: CreateProductDto): Observable<any> {
        const {date_updated, ...newProduct} = product
        return from(this.productRepository.update(id, newProduct))
    }

}
