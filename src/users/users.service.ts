import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Users } from './users.entity';
import { CreateUserDTO } from './dto/create-user.dto'; 
import { from, Observable } from 'rxjs';
import { UserRO } from './ro/users.ro';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(Users)
        private readonly userRepository: Repository<Users>
    ) {}

    public async findAll(): Promise<Users[]> {
        return await this.userRepository.find();
    }

    public async findByEmail(userEmail: string): Promise<Users | null> {
        return await this.userRepository.findOne({ email: userEmail });
    }

    public async findById(id: string): Promise<Users | null> {
        return await this.userRepository.findOneOrFail(id)
    }

    public async create(user: CreateUserDTO): Promise<Users> {
        return await this.userRepository.save(user);
    }

    public async update(id: string, newUser: CreateUserDTO): Promise<UserRO | null> {
        const user = await this.userRepository.findOneOrFail(id);

        if (!user.id) {
            console.error("No User Found");
            throw new HttpException('No User Found', HttpStatus.BAD_REQUEST);
        }

        await this.userRepository.update(id, newUser);
        const updatedUser = await this.userRepository.findOne(id);
        const {password, ...result} = updatedUser;
        return result
    }

    async delete(id: string): Promise<DeleteResult> {
        return await this.userRepository.delete(id);
    }

    public async register(userDto: CreateUserDTO): Promise<Users> {
        const { email } = userDto;
        let user = await this.userRepository.findOne({ where: { email } })

        if (user) {
            throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
        }

        user = await this.userRepository.create(userDto);
        return await this.userRepository.save(user);
    }

}
