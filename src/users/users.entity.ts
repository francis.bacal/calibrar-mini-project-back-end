import * as bcrypt from 'bcrypt';
import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, CreateDateColumn, UpdateDateColumn} from 'typeorm';
import { UserRO } from './ro/users.ro';
import { UserRole, UserType } from './users.interface';
import { debug, error } from 'console'

@Entity({name: "users"})
export class Users implements UserType {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({
        type: 'varchar',
        nullable: false,
        default: 'user'
    })
    role: UserRole;

    @Column({
        type: 'varchar',
        nullable: false,
        unique: false
    })
    fullName: string;

    @Column({
        type: 'varchar',
        nullable: false,
        unique: true
    })
    email: string;

    @Column({
        type: 'varchar',
        nullable: false
    })
    password: string;

    @CreateDateColumn()
    date_created: Date;

    @UpdateDateColumn()
    date_updated: Date;

    @BeforeInsert() 
    hashPassword() {
        if (!this.password) {
            debug('No password')
            return;
        }
        this.password = bcrypt.hashSync(this.password, 10);

    }

    @BeforeInsert()
    emailToLowerCase() {
        this.email = this.email.toLowerCase();
    }

    async comparePassword(input: string): Promise<boolean> {
        return await bcrypt.compare(input, this.password);
    }

    toResponseObject(showToken: boolean = true): UserRO {
        const { id, fullName, email, role } = this;

        const responseObj : UserRO = {
            id,
            fullName,
            email,
            role,
        };

        return responseObj;

    }
}