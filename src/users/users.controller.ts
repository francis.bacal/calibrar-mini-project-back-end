import { Body, Controller, Delete, Get, Param, Put, Res, UseGuards } from '@nestjs/common';
import { hasRoles } from 'src/auth/decorator/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { CreateUserDTO } from './dto/create-user.dto';
import { UserType } from './users.interface';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Get()
    findAll() {
        return this.usersService.findAll();
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.usersService.delete(id);
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Get(':id')
    findById(@Param('id') id: string) {
        return this.usersService.findById(id)
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Put(':id')
    update(@Param('id') id: string, @Body() user: CreateUserDTO) {
        return this.usersService.update(id, user);
    }
}
