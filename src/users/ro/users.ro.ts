import { IsEmail, IsNotEmpty } from 'class-validator';
import { UserRole } from '../users.interface';

export class UserRO {
    @IsNotEmpty()
    id: string;

    @IsNotEmpty()
    fullName: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    role: UserRole;
}