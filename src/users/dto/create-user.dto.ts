import { IsEmail, IsNotEmpty } from 'class-validator';
import { UserRole } from '../users.interface';

export class CreateUserDTO {
    @IsNotEmpty()
    id: string;

    @IsNotEmpty()
    fullName: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;

    @IsNotEmpty()
    role: UserRole;

    @IsNotEmpty()
    date_created: Date;

    @IsNotEmpty()
    date_updated: Date;
}