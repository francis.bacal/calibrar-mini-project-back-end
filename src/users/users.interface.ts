export interface UserType {
    id?: string;
    fullName?: string;
    email?: string;
    password?: string;
    role?: UserRole;
}

export enum UserRole {
    SUPERADMIN = "superAdmin",
    ADMIN = "admin",
    CLERK = "clerk",
    USER = "user"
}