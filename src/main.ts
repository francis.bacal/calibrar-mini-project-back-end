import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const PORT = configService.get('PORT');
  const baseRoute = `api/${configService.get('API_VERSION')}`;
  app.setGlobalPrefix(baseRoute);
  app.enableCors();

  await app.listen(PORT);
  console.log(`Server started running on http://localhost:${PORT}`, 'Bootstrap')
}
bootstrap();