
import * as Faker from "faker";
import { define } from "typeorm-seeding";
import { Users } from "../../../users/users.entity";

define(Users, (faker: typeof Faker) => {
  const firstName = faker.name.firstName()
  const lastName = faker.name.lastName()
  const fullName = `${firstName} ${lastName}`

  const user = new Users()
  user.fullName = fullName;
  user.email = faker.internet.email();
  user.password = faker.internet.password();
  return user
})