import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { users } from './data';
import * as bcrypt from 'bcrypt';
import { Products } from 'src/product/models/product.entity';

export default class CreateUsers implements Seeder {

    public async run(factory: Factory, connection: Connection): Promise<any> {
        await connection
            .createQueryBuilder()
            .insert()
            .into(Products)
            .values([
                {
                    name: "Pandesal",
                    price: 3
                },
                {
                    name: "Monay",
                    price: 5
                },
                {
                    name: "Ensaymada",
                    price: 15
                },
                {
                    name: "Egg Pie",
                    price: 25,
                    ingredients: ["Egg","Flour","Milk"]
                }
            ])
            .execute()
    }
}