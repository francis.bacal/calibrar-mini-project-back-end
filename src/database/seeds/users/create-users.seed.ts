import { Users } from '../../../users/users.entity';
import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import { users } from './data';
import * as bcrypt from 'bcrypt';

export default class CreateUsers implements Seeder {
    // public async run(factory: Factory, connection: Connection): Promise<void> {
    //     await factory(Users)()
    //         .map(async user => {
    //             console.log(user.fullName)

    //             return user;
    //         })
    //         .create();
    // }

    public async run(factory: Factory, connection: Connection): Promise<any> {
        await connection
            .createQueryBuilder()
            .insert()
            .into(Users)
            .values(
                users.map(user => {
                    user.password = bcrypt.hashSync(user.password, 10);
                    return user;
                })
            )
            .execute()
    }
}