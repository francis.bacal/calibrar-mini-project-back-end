import {MigrationInterface, QueryRunner, Table} from "typeorm";
import { UserRole } from '../../users/users.interface';

export class UsersTable1615447725395 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'users',
                columns: [
                    {
                        name: 'id',
                        type: 'uuid',
                        isPrimary: true,
                        isUnique: true,
                        isGenerated: true,
                        generationStrategy: 'uuid',
                        default: 'uuid_generate_v4()'
                    },
                    {
                        name: 'role',
                        type: 'varchar',
                        isNullable: false,
                        default: UserRole.USER
                    },
                    {
                        name: 'fullName',
                        type: 'varchar',
                        isNullable: false,
                        isUnique: false
                    },
                    {
                        name: 'email',
                        type: 'varchar',
                        isNullable: false,
                        isUnique: true
                    },
                    {
                        name: 'password',
                        type: 'varchar',
                        isNullable: false
                    }
                ]
            }), 
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`DROP TABLE users`);
    }

}
