import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Bacaery API';
  }

  testService(message?: string): string {
    return `Reached API: "${message ? message : ""}" - SUCCESS!`;
  }
}
