import { Body, Controller, Get, HttpStatus, Post, Req, Response, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDTO } from 'src/users/dto/create-user.dto';
import { LoginUserDTO } from 'src/users/dto/login-user.dto';
import { UsersService } from 'src/users/users.service';
import { AuthenticationService } from './auth.service';
import { hasRoles } from './decorator/roles.decorator';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { RolesGuard } from './guards/roles.guard';

@Controller('auth')
export class AuthenticationController {
    constructor(
        private readonly authenticationService: AuthenticationService,
        private readonly usersService: UsersService
    ) {}
    
    @hasRoles("superAdmin", "admin")
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Post('register')
    public async register(@Response() res, @Body() createUserDto: CreateUserDTO) {
        const result = await this.authenticationService.register(createUserDto);
        if(!result.success) {
            return res.status(HttpStatus.BAD_REQUEST).json(result);
        }

        return res.status(HttpStatus.OK).json(result);
    }

    @UseGuards(LocalAuthGuard)
    @Post('login')
    public async login(@Response() res, @Body() login: LoginUserDTO) {
        const user = await this.usersService.findByEmail(login.email);

        if (!user) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "User not found"})
        } else {
            const token = this.authenticationService.createToken(user);

            const result = {...token, ...user}
            const {password, date_created, date_updated, ...userData} = result 

            return res.status(HttpStatus.OK).json(userData);
        }
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    validate(@Req() req) {
        const {password, date_created, date_updated, ...user} = req.user

        return user;
    }
}
