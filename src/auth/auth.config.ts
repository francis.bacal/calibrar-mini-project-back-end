import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtOptionsFactory } from "@nestjs/jwt";

@Injectable()
export class AuthConfig implements JwtOptionsFactory {
    constructor(private readonly configService: ConfigService) {}

    createJwtOptions() {
        const options = {
            secret: this.configService.get('JWT_SECRET'),
            signOptions: { expiresIn: this.configService.get('EXPIRES_IN') }
        }

        return options
    }
}