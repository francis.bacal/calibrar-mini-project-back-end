import * as jwt from 'jsonwebtoken';
import { Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { Users } from '../users/users.entity';
import { CreateUserDTO } from '../users/dto/create-user.dto';
import { UserRO } from '../users/ro/users.ro';
import { JwtPayload, RegistrationStatus } from './interface/auth.interface';
import { ConfigService } from '@nestjs/config';


@Injectable()
export class AuthenticationService {
    constructor(
        private readonly usersService: UsersService,
        private readonly configService: ConfigService) {}

    private readonly logger = new Logger(AuthenticationService.name);
    
    async register(user: CreateUserDTO) {
        let status: RegistrationStatus = {
            success: true,
            message: 'User Registered'
        };

        // Register user with error catcher
        try {
            await this.usersService.register(user);
        } catch (err) {
            status = { success: false, message: err};
        }

        return status;
    }

    // Token generator with expiration
    createToken(user: Users) {
        // set expiration
        const expiration = this.configService.get('EXPIRES_IN');
        const secret_key  = this.configService.get('JWT_SECRET');

        const accessToken = jwt.sign(
            {
                id: user.id,
                email: user.email,
                fullName: user.fullName,
            },
            secret_key,
            { expiresIn: expiration}
        );

        return {expiration, accessToken}
    }

    // User Token Validator
    async validateUserToken(payload: JwtPayload): Promise<Users> {
        return await this.usersService.findById(payload.id);
    }

    // User local validator before login
    async validateUser(email: string, password: string): Promise<UserRO> {
        this.logger.log("Logging In...")
        const user = await this.usersService.findByEmail(email);

        if (user && await user.comparePassword(password)) {
            this.logger.log("Password Confirmed");
            const {password, ...result} = user;

            return result;
        }

        return null;
    }

    //
}
