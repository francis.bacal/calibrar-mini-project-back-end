import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { UsersService } from "src/users/users.service";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        private readonly usersService: UsersService    
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean>{
        // GET ROLES BY USING Reflector helper
        const roles = this.reflector.get<string[]>('roles', context.getHandler());

        if(!roles) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const requestor = request.user;
        const user = await this.usersService.findById(requestor.id)

        return roles.indexOf(user?.role) > -1;
    }
}