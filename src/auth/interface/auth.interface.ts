export interface JwtPayload {
    id: string;
    fullName: string;
    email: string;
}

export interface RegistrationStatus {
    success: boolean;
    message: string;
}

export interface IToken {
    readonly token: string;
}