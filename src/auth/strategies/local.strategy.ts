import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { AuthenticationService } from '../auth.service';


@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authenticationService: AuthenticationService) {
        super({
            usernameField: 'email',
            passwordField: 'password'
        });
    }

    private readonly logger = new Logger(AuthenticationService.name);

    async validate(username: string, password: string): Promise<any> {
        const user = await this.authenticationService.validateUser(username, password);
        this.logger.log(user ? "Authenticated" : "Log In Error");

        if (!user){
            throw new UnauthorizedException();
        }
        return user;
    }
}