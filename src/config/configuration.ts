import ormconfig from '../../ormconfig';

export default () => ({
    database: ormconfig
})