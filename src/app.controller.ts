import { Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from '@nestjs/passport';
import { config } from 'dotenv/types';
import { AppService } from './app.service';
import configuration from './config/configuration';

@Controller()
export class AppController {
  private version: string;
  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService
  ) { }


  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('users/login')
  logIn(@Res() res) {
    res.redirect(307, '/api/v1/auth/login')
  }

  @Post('users/register')
  register(@Res() res) {
    res.redirect(307, '/api/v1/auth/register')
  }

  // @Get('users')
  // testService() {
  //   this.version
  //   return this.appService.testService('users');
  // }

}
