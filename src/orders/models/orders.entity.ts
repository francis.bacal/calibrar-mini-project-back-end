import { BeforeInsert, Column, CreateDateColumn, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Products } from "src/product/models/product.entity";
import { Product } from "src/product/dto/product.dto";
import { Transaction } from "src/transaction/dto/transaction.dto";
import { Transactions } from "src/transaction/model/transaction.entity";


@Entity()
export class Orders {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne( () => Products, { eager: true })
    product: Product;

    @ManyToOne( () => Transactions, (transaction: Transactions) => transaction.orders, { cascade: ['insert', 'update', 'remove'] })
    transaction: Transaction;

    @Column( { type: "int", default: 1 })
    quantity: number

    @Column( { type: "int", nullable: false })
    total_price: number;

    @CreateDateColumn()
    date_created: Date;

    @UpdateDateColumn()
    date_updated: Date;

}