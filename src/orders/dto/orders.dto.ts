import { IsDate, IsInt, IsNotEmpty, IsString } from "class-validator";
import { Product } from "src/product/dto/product.dto";
import { CreateTransactionDto } from "src/transaction/dto/create-transaction.dto";
import { Transaction } from "src/transaction/dto/transaction.dto";


export class Order {

    @IsNotEmpty()
    @IsString()
    id: string;

    @IsNotEmpty()
    product: Product;

    @IsNotEmpty()
    transaction?: Transaction;

    @IsNotEmpty()
    @IsInt()
    quantity: number;
    
    @IsNotEmpty()
    @IsInt()
    total_price: number;

    @IsNotEmpty()
    @IsDate()
    date_created: Date;

    @IsNotEmpty()
    @IsDate()
    date_updated: Date;

}