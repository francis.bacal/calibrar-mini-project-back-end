import { IsDate, IsInt, IsNotEmpty, IsString } from "class-validator";
import { Product } from "src/product/dto/product.dto";
import { CreateTransactionDto } from "src/transaction/dto/create-transaction.dto";
import { Transaction } from "src/transaction/dto/transaction.dto";


export class CreateOrdersDto {
    @IsNotEmpty()
    @IsString()
    id: string;

    @IsNotEmpty()
    product: Product;

    @IsNotEmpty()
    transaction: CreateTransactionDto;

    @IsNotEmpty()
    @IsInt()
    quantity: number;
    
    @IsNotEmpty()
    @IsInt()
    total_price: number;

}