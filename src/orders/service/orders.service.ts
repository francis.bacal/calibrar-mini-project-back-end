import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { Transaction } from 'src/transaction/dto/transaction.dto';
import { Repository } from 'typeorm';
import { CreateOrdersDto } from '../dto/create-orders.dto';
import { Order } from '../dto/orders.dto';
import { Orders } from '../models/orders.entity';

@Injectable()
export class OrdersService {
    constructor(@InjectRepository(Orders) private readonly ordersRepository: Repository<Orders>) { }

    create(order: CreateOrdersDto): Observable<CreateOrdersDto> {
        
        //Validate total price
        Logger.debug(order.product.price)
        let total_price = order.product.price * order.quantity

        if (!order.total_price || order.total_price !== total_price) {
            order.total_price = total_price
        }

        return from(this.ordersRepository.save(order));
    }

    findAll(): Observable<Order[]> {
        return from(this.ordersRepository.find());
    }

    findById(id: string): Observable<Order> {
        return from(this.ordersRepository.findOne( { id } ) );
    }

    deleteByTransaction(_transaction: Transaction) {
        return from(this.ordersRepository.delete({transaction: _transaction}))
    }

    delete(id: string): Observable<any> {
        return from(this.ordersRepository.delete(id));
    }

    update(id: string, order: CreateOrdersDto): Observable<any> {
        return from(this.ordersRepository.update(id, order))
    }
}
