import { IsDate, IsInt, isInt, IsNotEmpty, IsString } from 'class-validator';
import { CreateOrdersDto } from 'src/orders/dto/create-orders.dto';
import { TransactionStatus } from '../model/transaction.types';

export class CreateTransactionDto {
    @IsNotEmpty()
    @IsString()
    id: string;

    @IsNotEmpty( {each: true } )
    orders: CreateOrdersDto[];

    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsInt()
    total_price: number;

    @IsNotEmpty()
    @IsInt()
    total_items: number;

    @IsNotEmpty()
    status?: TransactionStatus;

    @IsDate()
    date_created?: Date;

    @IsDate()
    date_updated?: Date;
}