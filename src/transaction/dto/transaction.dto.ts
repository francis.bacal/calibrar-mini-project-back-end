import { IsDate, IsNotEmpty } from 'class-validator';
import { TransactionStatus } from '../model/transaction.types';

export class Transaction {
    @IsNotEmpty()
    id: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    total_price: number;

    @IsNotEmpty()
    status?: TransactionStatus;

    @IsNotEmpty()
    @IsDate()
    date_created: Date;

    @IsNotEmpty()
    @IsDate()
    date_updated: Date;
}