// import { Sales } from "src/sales/models/sales.entity";
import { Logger } from "@nestjs/common";
import { Order } from "src/orders/dto/orders.dto";
import { Orders } from "src/orders/models/orders.entity";
import { UserRO } from "src/users/ro/users.ro";
import { Users } from "src/users/users.entity";
import { BeforeInsert, Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { TransactionStatus } from '../model/transaction.types';

@Entity()
export class Transactions {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @OneToMany( () => Orders, (order: Order) => order.transaction, { eager: true, cascade: ['insert', 'update'] })
    orders: Orders[];

    @Column( { type: "varchar", nullable: false })
    name: string;

    @Column( { type: "int", default: 0 } )
    total_price: number;

    @Column( { type: "int", default: 0 })
    total_items: number;

    @Column( { type: 'enum', enum: TransactionStatus, default: TransactionStatus.PENDING })
    status: TransactionStatus;
    
    @CreateDateColumn()
    date_created: Date;

    @UpdateDateColumn()
    date_updated: Date;
}