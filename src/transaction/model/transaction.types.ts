
export enum TransactionStatus {
    PENDING,
    PAID,
    PREPARING,
    PICKUP,
    DONE,
    VOID
}