import { Module } from '@nestjs/common';
import { TransactionService } from './service/transaction.service';
import { TransactionController } from './controller/transaction.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transactions } from './model/transaction.entity';
import { OrdersModule } from 'src/orders/orders.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Transactions]),
    OrdersModule
  ],
  providers: [TransactionService],
  controllers: [TransactionController]
})
export class TransactionModule {}
