import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { hasRoles } from 'src/auth/decorator/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { CreateOrdersDto } from 'src/orders/dto/create-orders.dto';
import { OrdersService } from 'src/orders/service/orders.service';
import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { Transaction } from '../dto/transaction.dto';
import { TransactionService } from '../service/transaction.service';

@Controller('transaction')
export class TransactionController {
    constructor(
        private readonly transactionService: TransactionService,
        private readonly ordersService: OrdersService
    ) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    create(@Body() transaction: CreateTransactionDto) {

        // Throw error 400 if no orders
        if (!transaction.orders || transaction.orders.length === 0 || transaction.orders === undefined) {
            throw new HttpException(`Error occured: No Orders to transact`, HttpStatus.BAD_REQUEST);
        }

        // Check total items and total price
        let ordersPriceSum: number = 0;
        transaction.orders.map( order => {
            return ordersPriceSum += order.total_price;
        })

        if (transaction.total_items !== transaction.orders.length) {
            transaction.total_items = transaction.orders.length;
        }

        if (transaction.total_price === 0 || transaction.total_price !== ordersPriceSum) {
            transaction.total_price = ordersPriceSum;
        }
        
        // Add transaction
        return this.transactionService.create(transaction).pipe(
            catchError(err => {
                let message: string;

                if (err.code && Number(err.code) == 23505) {
                    message = "Duplicate value found in database";
                } else if (Number(err.code) === 23502) {
                    message = "Invalid transaction data. Check input"
                } else {
                    message = err.message
                }

                throw new HttpException(`Error occured: ${err.message}`, HttpStatus.BAD_REQUEST);
            })
        )
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    findAll() {
        return this.transactionService.findAll();
    }

    @UseGuards(JwtAuthGuard)
    @Get(':id')
    findById(@Param('id') id: string) {
        return this.transactionService.findById(id);
    }

    @hasRoles('superAdmin', 'admin')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Delete(':id')
    delete(@Param('id') id: string, @Body() transaction: Transaction) {
        this.ordersService.deleteByTransaction(transaction)
        return this.transactionService.delete(id);
    }

    @UseGuards(JwtAuthGuard)
    @Put(':id')
    update(@Param('id') id: string, @Body() transaction: CreateTransactionDto): Observable<any> {
        return this.transactionService.update(id, transaction)
    }

}
