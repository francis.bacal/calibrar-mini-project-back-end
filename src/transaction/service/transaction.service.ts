import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { Repository } from 'typeorm';
import { CreateTransactionDto } from '../dto/create-transaction.dto';
import { Transaction } from '../dto/transaction.dto';
import { Transactions } from '../model/transaction.entity';

@Injectable()
export class TransactionService {
    constructor(@InjectRepository(Transactions) private readonly transactionRepository: Repository<Transactions>
    ) {}

    create(transaction: CreateTransactionDto): Observable<CreateTransactionDto> {
        let total_price = 0
        transaction.orders = transaction.orders.map( order => {
           order.total_price = order.quantity * order.product.price;
           total_price += order.total_price;
           return order;
        });

        transaction.total_price = total_price;
        
        return from(this.transactionRepository.save(transaction))
    }

    findAll(): Observable<Transaction[]> {
        return from(this.transactionRepository.find())
    }

    findById(id: string): Observable<Transaction> {
        return from(this.transactionRepository.findOne({id}));
    }

    delete(id: string): Observable<any> {
        return from(this.transactionRepository.delete(id));
    }

    update(id:string, transaction: CreateTransactionDto): Observable<any> {
        return from(this.transactionRepository.update(id, transaction));
    }
}
