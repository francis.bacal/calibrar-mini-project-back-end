import * as dotenv from 'dotenv';
dotenv.config();

export default {
    type: process.env.DB_CONNECTION,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: [__dirname + '/../**/*.entity.js'],
    synchronize: true,
    migrations: ["dist/src/database/migrations/*{.ts,.js}"],
    migrationsTableName: process.env.DB_MIGRATIONS_TABLE_NAME,
    migrationsRun: true,
    seeds: [__dirname + '/../**/*{.seed.js, .seed.ts}'],
    factories: [__dirname + '/../**/*{.factory.js, .factory.ts}'],
    cli: {
        migrationsDir: "src/database/migrations"
    }
}